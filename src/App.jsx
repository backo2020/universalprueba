import './styles/stylesComponent/index.scss'
import './styles/stylesPages/index.scss'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import React from 'react'
import {Products} from './Pages/products/Products'
import {Sidebar} from './components/Sidebar'

function App() {
  return (
    <div>
      <Router>
        <Sidebar>
        <Routes>
          <Route path='/' element={<Products />}  />
        </Routes>
        </Sidebar >
      </Router>
    </div>
      
      
  );
}

export default App;
