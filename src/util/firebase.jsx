
import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'


const firebaseConfig = {
  apiKey: "AIzaSyDB0tiK1Y2DeT0I5UtP50rmOn3fZjJeVKs",
  authDomain: "universal-7685b.firebaseapp.com",
  databaseURL: "https://universal-7685b-default-rtdb.firebaseio.com",
  projectId: "universal-7685b",
  storageBucket: "universal-7685b.appspot.com",
  messagingSenderId: "814158109638",
  appId: "1:814158109638:web:461a1102ad504a41feb0c7"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const firebaseNow = firebase.firestore.FieldValue.serverTimestamp();

export {db, auth, firebase, storage, firebaseNow}
