import axios from 'axios'
import { GET_PRODUCTS, DELETE_PRODUCTS, POST_PRODUCTS, PUT_PRODUCTS } from './global'

const getProducts = async (categoria, nombre, descripcion) => {
    let category = categoria ? categoria : "0"
    let name = nombre ? nombre : "-1"
    let description = descripcion ? descripcion : "-1"
    let result = [];
    await axios.get(GET_PRODUCTS + `?categoriaID=${category}&nombre=${name}&descripcion=${description}`)
        .then((res) => {
            console.log(res.data);
            result = res.data
        }).catch((err) => {
            console.log(err);
        })
    return result
}

const deleteProducts = async (idProducto) => {
    let result = ""
    await axios.delete(DELETE_PRODUCTS + idProducto)
        .then((res) => {
            result = "ok"
        }).catch((err) => {
            result = err.response.status
        })
    return result
}

const updateProducts = async (id,nombre,descripcion,categoriaId,stock,precio,imagen) => {
    let result = ""
    await axios.put(PUT_PRODUCTS, {
        id: id,
        nombre: nombre,
        descripcion: descripcion,
        categoriaId: categoriaId,
        imagen: imagen,
        stock: stock,
        precio: precio
    })
        .then((res) => {
            result = "ok"
        }).catch((err) => {
            result = err.response.status
        })
    return result
}

const newProducts = async (newName,newDescripcion,newCategoria,newStock,newPrecio,imagen) => {
    let result = ""
    await axios.post(POST_PRODUCTS, {
        id: 0,
        nombre: newName,
        descripcion: newDescripcion,
        categoriaId: newCategoria,
        imagen: imagen,
        stock: newStock,
        precio: newPrecio
    })
        .then((res) => {
            result = "ok"
        }).catch((err) => {
            result = err.response.status
        })
    return result
}

export default { getProducts, deleteProducts,updateProducts,newProducts }



