//PRUEBAS
// export const URL_BASE = 'https://localhost:7022/'
//PRODUCCION
export const URL_BASE = 'https://www.cruduniversal.somee.com/'

export const GET_PRODUCTS = URL_BASE + 'AdminProduct/getProducts'
export const DELETE_PRODUCTS = URL_BASE + 'AdminProduct/deleteProduct/'
export const POST_PRODUCTS = URL_BASE + 'AdminProduct/CreateNewProduct'
export const PUT_PRODUCTS = URL_BASE + 'editProduct'