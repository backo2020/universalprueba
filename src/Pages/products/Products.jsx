import React, { useState } from 'react'
import Card from '../../components/Card'
import functions from '../../util/AuthProducts'
import ListProduct from './ListProduct'
import swal from 'sweetalert';
import { FiUploadCloud, FiUserCheck } from "react-icons/fi";
import {storage} from '../../util/firebase'

export const Products = () => {
  const [listProducts, setListProducts] = useState()
  const [category, setCategory] = useState()
  const [name, setName] = useState()
  const [description, setDescription] = useState()
  const [newName, setNewName] = useState("")
  const [newDescripcion, setNewDescripcion] = useState("")
  const [newCategoria, setNewCategoria] = useState("")
  const [newStock, setNewStock] = useState("")
  const [newPrecio, setNewPrecio] = useState("")
  const [error, setError] = useState("")
  const [activeError, setActiveError] = useState(false)
  const [addImagen, setAddImagen] = useState()
  const [urlProducto, setUrlProducto] = useState("")

  const getProduct = async (categoria, nombre, descripcion) => {
    let res = await functions.getProducts(categoria, nombre, descripcion)
    setListProducts(res)
  }

  const findProduct = (e) => {
    e.preventDefault()
    getProduct(category, name, description)
  }

  const newProduct = async (e) => {
    e.preventDefault()
    if (!newName.trim()) {
      setError("debe ingresar el nombre")
      setActiveError(true)
      return
    }
    if (!newDescripcion.trim()) {
      console.log("entra");
      setError("debe ingresar una descripcion")
      setActiveError(true)
      return
    }
    if (!newCategoria.trim()) {
      setError("debe ingresar una categoria")
      setActiveError(true)
      return
    }
    if (!newStock.trim()) {
      setError("debe ingresar el stock")
      setActiveError(true)
      return
    }
    if (!newPrecio.trim()) {
      setError("debe ingresar el precio")
      setActiveError(true)
      return
    }
    if (!urlProducto.trim()) {
      setError("debe ingresar la imagen del producto")
      setActiveError(true)
      return
    }
    setActiveError(false)
    let res = await functions.newProducts(newName, newDescripcion, newCategoria, newStock, newPrecio, urlProducto)
    if (res = "ok") {
      swal("", "Producto creado correctamente", "success");

      getProduct()

    }
  }

  const subirImagen = async (e) => {
    setAddImagen(e)
    const img = e.target.files[0]
    const imagenRef = await storage.ref().child("imgProductos").child(e.target.files[0].name)
    await imagenRef.put(img)
    const imagenURL = await imagenRef.getDownloadURL()
    setUrlProducto(imagenURL)
  }

  const reset = () => {
    setNewName("")
    setNewDescripcion("")
    setNewStock("")
    setNewPrecio("")
    setNewCategoria("")
    setAddImagen(null)
  }

  useState(async () => {
    getProduct("0", "-1", "-1")
  }, [])
  return (

    <div className="container-notification-form">
      <div className='card-notification-form'>
        <Card>
          <div className="notify-card__title">
            <h1>Productos</h1>
          </div>
          <br />
          <Card>
            <div className="notify-card__title">
              <h3>Crear Producto</h3>
            </div>
            <br />
            <form>

              <div>
                <label className='label-form'> Nombre :
                  <input className='input-form' type='text' onChange={e => setNewName(e.target.value)} value={newName}>
                  </input>
                </label >

                <label className='label-form'> Descripcion :
                  <input className='input-form' type='text' onChange={e => setNewDescripcion(e.target.value)} value={newDescripcion}>
                  </input>
                </label>

                <label className='label-form'> Categoria :
                  <input className='input-form' type='text' onChange={e => setNewCategoria(e.target.value)} value={newCategoria}>
                  </input>
                </label >
              </div>

              <div style={{ marginTop: "10px", marginLeft: "20px" }}>
                <label className='label-form'> Stock :
                  <input className='input-form' type='text' onChange={e => setNewStock(e.target.value)} value={newStock}>
                  </input>
                </label >

                <label className='label-form' style={{ marginLeft: "38px" }}> Precio :
                  <input className='input-form' type='text' onChange={e => setNewPrecio(e.target.value)} value={newPrecio}>
                  </input>
                </label >

                <label className='label-form' style={{ marginLeft: "18px" }}> Imagen :
                  {addImagen ? (
                     <> {addImagen.target.files[0].name}  <FiUserCheck size={18} style={{ color: "green" }}/> </>
                  ) : (
                    
                      <label>
                        <input
                          style={{ display: "none"}}
                          type="file"
                          onChange={e => subirImagen(e)}
                        />
                        <FiUploadCloud size={40} style={{ cursor: "pointer", color: "#FFD22A", marginLeft:"5%",marginTop:"5px"  }} />
                      </label>
                    
                  )}
                </label >
              </div>

              <div className='notify-buttons'>
                <div style={{ display: "flex", marginLeft: "-40px" }}>
                  <button type="button" className='button' onClick={newProduct}>Crear</button>
                  <button type="button" className='button' onClick={reset}>Limpiar</button>
                </div>
              </div>
              <div>
                {activeError ? (
                  <div style={{ marginBotton: "10px", color: "red", marginLeft: "40%" }}>{error} !</div>
                ) : null}
              </div>

            </form>
          </Card>
          <br />
          <Card>
            <div className="notify-card__title">
              <h3>Buscar Producto</h3>
            </div>
            <br />
            <form>
              <label className='label-form'> Categoria :
                <input className='input-form' type='text' onChange={e => setCategory(e.target.value)}>
                </input>
              </label >

              <label className='label-form'> Nombre :
                <input className='input-form' type='text' onChange={e => setName(e.target.value)}>
                </input>
              </label >

              <label className='label-form'> Descripcion :
                <input className='input-form' type='text' onChange={e => setDescription(e.target.value)}>
                </input>
              </label>

              <div className='notify-buttons'>
                <div style={{ display: "flex", marginLeft: "-40px" }}>
                  <button type="button" className='button' onClick={findProduct}>Buscar</button>
                </div>
              </div>
            </form>
          </Card>
          <br />
          <ListProduct data={listProducts} />

        </Card>
      </div>
    </div>

  )
}
