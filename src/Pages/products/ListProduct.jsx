import React, { useState } from 'react'
import { BsTrash, BsPencil } from "react-icons/bs";
import { Table, TableBody, TableContainer, TableHead, TableRow } from '@mui/material'
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import TablePagination from '@mui/material/TablePagination';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import LastPageIcon from '@mui/icons-material/LastPage';
import IconButton from '@mui/material/IconButton';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { AiOutlineSave } from "react-icons/ai";
import functions from '../../util/AuthProducts'
import swal from 'sweetalert';
import { storage } from '../../util/firebase'
import { FiUploadCloud, FiUserCheck } from "react-icons/fi";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.white,
        color: "#f87d20",
        fontSize: 18,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;
    


    const handleFirstPageButtonClick = (
        event
    ) => {
        onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    


    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </Box>
    );
}

const ListProduct = (props) => {
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [page, setPage] = React.useState(0);
    const [id, setid] = useState()
    const [name, setName] = useState()
    const [descripcion, setDescripcion] = useState()
    const [categoria, setCategoria] = useState()
    const [stock, setStock] = useState()
    const [precio, setPrecio] = useState()
    const [addImagen, setAddImagen] = useState()
    const [urlImagen, setUrlImagen] = useState()
    const [ChangeImage,setChangeImage] = useState(false)

    const handleChangePage = (
        event,
        newPage
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - props.data.length) : 0;

    const activeEdition = (item) => {
        setid(item.id)
        setName(item.nombre)
        setDescripcion(item.descripcion)
        setCategoria(item.categoriaId)
        setStock(item.stock)
        setPrecio(item.precio)
        setUrlImagen(item.imagen)
    }

    const savedChanges = async () => {
        if(ChangeImage == true){
            const img = addImagen.target.files[0]
            const imagenRef = await storage.ref().child("imgProductos").child(addImagen.target.files[0].name)
            await imagenRef.put(img)
            const imagenURL = await imagenRef.getDownloadURL()
            if(imagenURL){
                let res = await functions.updateProducts(id, name, descripcion, categoria, stock, precio, imagenURL)
                if (res = "ok") {
                    swal("", "Producto actualizado correctamente", "success");
                    setChangeImage(false)
                    setTimeout(() => {
                        window.location.reload()
                    }, 1500);
                }
            }else{
                let res = await functions.updateProducts(id, name, descripcion, categoria, stock, precio, urlImagen)
                if (res = "ok") {
                    swal("", "Producto actualizado correctamente", "success");
                    setChangeImage(false)
                    setTimeout(() => {
                        window.location.reload()
                    }, 1500);
                }
            }
        }else{
            let res = await functions.updateProducts(id, name, descripcion, categoria, stock, precio, urlImagen)
                if (res = "ok") {
                    swal("", "Producto actualizado correctamente", "success");
                    setChangeImage(false)
                    setTimeout(() => {
                        window.location.reload()
                    }, 1500);
                }
        }
    }

    const deleteSector = async (item) => {
        let res = await functions.deleteProducts(item)
        if (res = "ok") {
            swal("", "Producto eliminado correctamente", "success");
            setTimeout(() => {
                window.location.reload()
            }, 1500);
        }
    }

    const subirImagen = async (e) => {
        setAddImagen(e)
        setChangeImage(true)
      }


    return (
        <div>
            {props.data ? (
                <TableContainer component={Paper}>

                    <Table sx={{ minWidth: 1300 }} aria-label="custom pagination table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center"> Imagen </StyledTableCell>
                                <StyledTableCell align="left"> Nombre </StyledTableCell>
                                <StyledTableCell align="left"> Descripcion </StyledTableCell>
                                <StyledTableCell align="left"> Categoria </StyledTableCell>
                                <StyledTableCell align="center"> Stock </StyledTableCell>
                                <StyledTableCell align="center"> Precio </StyledTableCell>
                                <StyledTableCell align="left"> Opciones </StyledTableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {(rowsPerPage > 0
                                ? props.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : props.data
                            )
                                .map((item) => (
                                    <StyledTableRow
                                        key={item.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        {id == item.id ? (
                                            <label> 
                                            {addImagen ? (
                                               <> {addImagen.target.files[0].name}  <FiUserCheck size={18} style={{ color: "green", marginTop:"20px" }}/> </>
                                            ) : (
                                                <label>
                                                  <input
                                                    style={{ display: "none"}}
                                                    type="file"
                                                    onChange={e => subirImagen(e)}
                                                  />
                                                  <FiUploadCloud size={40} style={{ cursor: "pointer", color: "#FFD22A", marginLeft:"30%",marginTop:"5px"  }} />
                                                </label>
                                            )}
                                          </label >
                                        ) : (
                                            <StyledTableCell align="center" style={{ width: 100 }}>
                                                <div>
                                                    <img loading='lazy' width={80} src={item.imagen} />
                                                </div>
                                            </StyledTableCell>
                                        )}
                                        {id == item.id ? (
                                            <StyledTableCell align="left" component="th" scope="row" >
                                                <input
                                                    className='inputEdit'
                                                    onChange={e => setName(e.target.value)}
                                                    value={name}
                                                />
                                            </StyledTableCell>
                                        ) : (
                                            <StyledTableCell align="left" component="th" scope="row" style={{ width: 120 }}>{item.nombre}</StyledTableCell>
                                        )}

                                        {id == item.id ? (
                                            <StyledTableCell align="left" component="th" scope="row" >
                                                <input
                                                    className='inputEdit'
                                                    onChange={e => setDescripcion(e.target.value)}
                                                    value={descripcion}
                                                />
                                            </StyledTableCell>
                                        ) : (
                                            <StyledTableCell align="left" component="th" scope="row" style={{ width: 120 }}>{item.descripcion}</StyledTableCell>
                                        )}

                                        {id == item.id ? (
                                            <StyledTableCell align="left" component="th" scope="row" >
                                                <input
                                                    className='inputEdit'
                                                    onChange={e => setCategoria(e.target.value)}
                                                    value={categoria}
                                                />
                                            </StyledTableCell>
                                        ) : (
                                            <StyledTableCell align="left" component="th" scope="row" style={{ width: 120 }}>{item.categoriaId}</StyledTableCell>
                                        )}

                                        {id == item.id ? (
                                            <StyledTableCell align="center" component="th" scope="row" >
                                                <input
                                                    className='inputEdit'
                                                    type="number"
                                                    onChange={e => setStock(e.target.value)}
                                                    value={stock}
                                                />
                                            </StyledTableCell>
                                        ) : (
                                            <StyledTableCell align="center" component="th" scope="row" style={{ width: 120 }}>{item.stock}</StyledTableCell>
                                        )}

                                        {id == item.id ? (
                                            <StyledTableCell align="center" component="th" scope="row" >
                                                <input
                                                    className='inputEdit'
                                                    type="number"
                                                    onChange={e => setPrecio(e.target.value)}
                                                    value={precio}
                                                />
                                            </StyledTableCell>
                                        ) : (
                                            <StyledTableCell align="center" component="th" scope="row" style={{ width: 100 }}>
                                                {" "}
                                                {"$ " +
                                                    new Intl.NumberFormat("en", {
                                                        style: "decimal",
                                                        currency: "COP",
                                                    }).format(item.precio)}
                                            </StyledTableCell>
                                        )}

                                        <StyledTableCell align="left" style={{ width: 140 }}>
                                            {id == item.id ? (
                                                <AiOutlineSave style={{ color: "#576979", cursor: "pointer", marginRight: "20%" }} size="22" onClick={() => savedChanges()} />
                                            ) :
                                                (
                                                    <BsPencil style={{ color: "green", cursor: "pointer", marginRight: "20%" }} size="22" onClick={() => activeEdition(item)} />
                                                )}
                                            <BsTrash style={{ color: "red", cursor: "pointer" }} size="22" onClick={() => deleteSector(item.id)} />
                                        </StyledTableCell>
                                    </StyledTableRow >
                                ))}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 67 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>

                    </Table>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25, { label: 'Todo', value: -1 }]}
                        colSpan={3}
                        count={props.data.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        SelectProps={{
                            inputProps: {
                                'aria-label': 'rows per page',
                            },
                            native: true,
                        }}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        ActionsComponent={TablePaginationActions}
                    />
                </TableContainer>
            ) : ""
            }
        </div >
    )
}

export default ListProduct