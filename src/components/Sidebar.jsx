import React, { useState } from 'react'
import { motion } from 'framer-motion'
import { NavLink } from 'react-router-dom'
import { BiHomeSmile } from "react-icons/bi";
import { FaBars } from "react-icons/fa";

import logo from '../assets/IconoLoading.png'

const routes = [{
  path: "/",
  name: "Productos",
  icon: <BiHomeSmile />
}
]

export const Sidebar = ({ children }) => {
  const [isOpen, setIsopen] = useState(true)
  const toggle = () => setIsopen(!isOpen)
  return (
    <React.Fragment>
      <div className='sidebar_container'>
        <motion.div animate={{ width: isOpen ? "200px" : "45px" }} className="sidebar">
          <div className='top_section'>
            {isOpen ? (
              <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
              </header>
            ) : null}
            <div className={!isOpen ? 'bars' : 'barsOpen'}>
              <FaBars onClick={toggle} />
            </div>
          </div>
          <section className='routes'>
            {routes.map((route) => (
              <NavLink to={route.path} key={route.name} className="linksidebar">
                <div className='icon'>{route.icon}</div>
                <div className='link-text'>{route.name}</div>
              </NavLink>
            ))}
          </section>
        </motion.div>
        <main>{children}</main>
      </div>
    </React.Fragment>
  )
}
